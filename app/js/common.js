
    var chr1 = document.getElementById('myChart1');

    //fonts
    Chart.defaults.global.defaultFontColor = '#000000';
    Chart.defaults.global.defaultFontFamily = 'Open Sans';
    Chart.defaults.global.defaultFontSize = 12;

    var myChart1 = new Chart(chr1, {

        type: 'bar',
        data: {
            labels: ['янв', 'фев', 'март', 'апр', 'май', 'июнь', 'иьюль', 'авг', 'сен', 'окт', 'ноя', 'дек'],
            datasets: [
                {
                data: [12, 19, 3, 5, 2, 3, 2, 1, 3, 2, 1, 2],
                backgroundColor: '#1E87F0',
                },
                {
                    data: [12, 19, 3, 5, 2, 3, 2, 1, 3, 2, 1, 2],
                    backgroundColor: '#F0506E'
                }
            ]
        },
        options: {
            legend: {
              display: false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            maintainAspectRatio: false
        }
    });


    var chr2 = document.getElementById('myChart2');

    //fonts
    Chart.defaults.global.defaultFontColor = '#000000';
    Chart.defaults.global.defaultFontFamily = 'Open Sans';
    Chart.defaults.global.defaultFontSize = 10;

    var myChart2 = new Chart(chr2, {
        type: 'bar',
        data: {
            labels: ['Финансовые', 'Почтовые', 'EMS', 'Услуга 4', 'Услуга 5', 'Услуга 6'],
            datasets: [
                {
                    label: 'план',
                    data: [12, 19, 3, 5, 2, 3, 2, 1, 3, 2, 1, 2],
                    backgroundColor: '#1E87F0',
                },
                {
                    label: 'факт',
                    data: [12, 19, 3, 5, 2, 3, 2, 1, 3, 2, 1, 2],
                    backgroundColor: '#F0506E'
                }
            ]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            maintainAspectRatio: false
        }
    });


    var ctx = document.getElementById('chart');

    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: ['Алматы', 'Нур-Султан', 'Шымкент','Актобе','Караганда','Тараз','Павлодар','Усть-Каменогорск','Семей','Атырау','Костанай','Кызыл-Орда','Уральск','Петропавловск','Актау','Талдыкорган'],
            datasets: [
                {
                    label: 'Факт',
                    data: [12, 80, 80, 75, 73,70, 65, 60, 58, 50, 47, 42, 38, 36, 33, 30],
                    backgroundColor: '#F0506E' // red

                },
                {
                    label: 'План',
                    data: [88, 20, 20, 25, 27, 30, 35, 40, 42, 50, 53, 58, 62, 64, 67, 70 ],
                    backgroundColor: '#1E87F0' // green
                }
            ]
        },
        options: {
            scales: {
                xAxes: [{ stacked: true }],
                yAxes: [{ stacked: true }]
            },
            maintainAspectRatio: true,
            legend: {
                position: 'bottom'
            }
        }
    });

    var login = "1";
    var password = "1";

    var plan = {
        "async": true,
        "crossDomain": true,
        "url": "http://demo7.arta.kz/Synergy/rest/api/registry/count_data?registryCode=customers_registry_plans&fields=crm_form_plan_all_count_fact&fieldCode=crm_form_plan_all_sum_fact&countType=sum",
        "method": "GET",
        "headers": {
            "authorization": ("Basic " + btoa(login + ":" + password))
        }
    };

    var fact = {
        "async": true,
        "crossDomain": true,
        "url": "http://demo7.arta.kz/Synergy/rest/api/registry/count_data?registryCode=customers_registry_contracts&fields=crm_form_contract_actual_sum&fieldCode=crm_form_contract_actual_sum&countType=sum",
        "method": "GET",
        "headers": {
            "authorization": ("Basic " + btoa(login + ":" + password))
        }
    };


    $('.sq-price').hide();
    $('.change').on('click',
        function() {
            $('.sq-price, .none-sq-price').toggle(200);
        }
    );





